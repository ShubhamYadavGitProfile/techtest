import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  loginForm!:FormGroup;
  submitted:boolean = false;
  Email: string = 'info@optium.com';
  Password:string = 'Optium@112233';
  message:string='';

  constructor(private fb :FormBuilder, private router : Router) { }
  get f() { return this.loginForm.controls; }
 

  ngOnInit(): void {
    this.loginForm = this.fb.group({
    email:['',[Validators.required,Validators.email]],
    password:['',Validators.required,],
    });
  }


  submit(){
  this.submitted = true;
  //   // \"email\": \"info@optium.com\",\n    \"password\": \"Optium@112233\"\n}
    let email = this.loginForm.value.email;
    let password = this.loginForm.value.password;
    if(email == this.Email && password == this.Password)
    {
      console.log('YES');
   this.router.navigateByUrl('/dashboard');
    }
    else{
    this.message = 'Please login with correct details!!';
    }
  }
}

